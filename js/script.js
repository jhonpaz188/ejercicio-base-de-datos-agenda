$(document).ready(function() {
    $('#boton1').click(function() {
        $.ajax({
            url: "php/consulta.php",
            success: function(result) {
                $("#resultado").html(result);
            }
        });
    });

    $('#boton2').click(function() {
        $.ajax({
            url: "php/agregar.php",
            success: function(result) {
                $("#resultado").html(result);
            }
        });
    });

    $('.objeto1').click(function(e) {
        e.preventDefault();
        id = $(this).attr('id');
        var datos = {
            id: id,
        }
        $.ajax({
            data: datos, //datos que se envian a traves de ajax
            url: "php/editar.php",
            type: 'post',
            success: function(result) {
                $("#resultado").html(result);
            }
        });

    });

    $('.objeto2').click(function(e) {
        e.preventDefault();
        id = $(this).attr('id');
        var datos = {
            id: id,
        }
        $.ajax({
            data: datos, //datos que se envian a traves de ajax
            url: "php/eliminar.php",
            type: 'post',
            success: function(result) {
                $("#resultado").html(result);
            }
        });

    });

    $('#actualizar').click(function(e) {
        e.preventDefault();

        id = $('#id2').val();
        nombre = $('#nombre2').val();
        apellido = $('#apellido2').val();
        edad = $('#edad2').val();
        telefono = $('#telefono2').val();
        var datos = {
            id: id,
            nombre: nombre,
            apellido: apellido,
            edad: edad,
            telefono: edad,

        }
        $.ajax({
            data: datos, //datos que se envian a traves de ajax
            url: "php/ejecuta_editar.php",
            type: 'post',
            success: function(result) {
                $("#resultado").html(result);
            }
        });
    });

    $('#agregar').click(function(e) {
        e.preventDefault();

        nombre = $('#nombre').val();
        apellido = $('#apellido').val();
        edad= $('#edad').val();
        telefono= $('#telefono').val();
        var datos = {
            nombre: nombre,
            apellido: apellido,
            edad: edad,
            telefono: telefono,
        }

        $.ajax({
            data: datos, //datos que se envian a traves de ajax
            url: "php/ejecuta_agregar.php",
            type: 'post',
            success: function(result) {
                $("#resultado").html(result);
            }
        });
        nombre = "";
        apellido = "";
    });

});